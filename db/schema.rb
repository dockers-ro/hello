# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170109170657) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "barcodes", force: :cascade do |t|
    t.string   "name"
    t.string   "sequence"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "status"
    t.integer  "lab_id"
    t.integer  "idx"
    t.index ["lab_id"], name: "index_barcodes_on_lab_id", using: :btree
  end

  create_table "labs", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "status"
    t.text     "options"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "medics", force: :cascade do |t|
    t.string   "encrypted_first_name"
    t.string   "encrypted_last_name"
    t.string   "encrypted_email"
    t.string   "encrypted_clinic"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "lab_id"
    t.string   "encrypted_first_name_iv"
    t.string   "encrypted_last_name_iv"
    t.string   "encrypted_email_iv"
    t.string   "encrypted_clinic_iv"
    t.index ["lab_id"], name: "index_medics_on_lab_id", using: :btree
  end

  create_table "patients", force: :cascade do |t|
    t.string   "encrypted_first_name"
    t.string   "encrypted_last_name"
    t.string   "encrypted_email"
    t.string   "encrypted_city"
    t.string   "encrypted_county"
    t.string   "encrypted_address"
    t.integer  "medic_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "encrypted_birth_date"
    t.integer  "lab_id"
    t.string   "encrypted_first_name_iv"
    t.string   "encrypted_last_name_iv"
    t.string   "encrypted_email_iv"
    t.string   "encrypted_city_iv"
    t.string   "encrypted_county_iv"
    t.string   "encrypted_address_iv"
    t.string   "encrypted_birth_date_iv"
    t.index ["lab_id"], name: "index_patients_on_lab_id", using: :btree
    t.index ["medic_id"], name: "index_patients_on_medic_id", using: :btree
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.string   "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sequence_runs", force: :cascade do |t|
    t.string   "lis_reference"
    t.string   "run_folder"
    t.string   "sequencing_metrics_profile"
    t.string   "barcode_set"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "lab_id"
    t.string   "status",                     default: "active"
    t.string   "tracer_set"
    t.index ["lab_id"], name: "index_sequence_runs_on_lab_id", using: :btree
  end

  create_table "sequencing_samples", force: :cascade do |t|
    t.integer  "sequence_run_id"
    t.string   "barcode_sequence"
    t.string   "sequencing_sample_metrics_profile"
    t.string   "sequencing_sample_id"
    t.string   "target_sets"
    t.string   "lanes"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.string   "status",                            default: "active"
    t.string   "tracer_sequence"
    t.index ["sequence_run_id"], name: "index_sequencing_samples_on_sequence_run_id", using: :btree
  end

  create_table "tests", force: :cascade do |t|
    t.integer  "patient_id"
    t.string   "status"
    t.string   "encrypted_sequencing_sample_id"
    t.string   "encrypted_medical_record"
    t.string   "encrypted_case_file"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "barcode_sequence"
    t.integer  "lab_id"
    t.string   "tracer_sequence"
    t.string   "sequencing_sample_metrics_profile"
    t.string   "product"
    t.string   "sample_type"
    t.string   "depth"
    t.string   "has_plasma"
    t.string   "has_tracers"
    t.integer  "barcode_index"
    t.integer  "tracer_index"
    t.string   "barcode_set"
    t.string   "tracer_set"
    t.string   "encrypted_samples_collected"
    t.string   "encrypted_samples_received"
    t.string   "encrypted_maternal_age"
    t.string   "encrypted_gestational_age"
    t.string   "encrypted_maternal_age_iv"
    t.string   "encrypted_gestational_age_iv"
    t.string   "encrypted_sequencing_sample_id_iv"
    t.string   "encrypted_medical_record_iv"
    t.string   "encrypted_case_file_iv"
    t.string   "encrypted_samples_collected_iv"
    t.string   "encrypted_samples_received_iv"
    t.index ["lab_id"], name: "index_tests_on_lab_id", using: :btree
    t.index ["patient_id"], name: "index_tests_on_patient_id", using: :btree
  end

  create_table "tracers", force: :cascade do |t|
    t.integer  "lab_id"
    t.string   "status"
    t.string   "name"
    t.string   "reference_name"
    t.integer  "start_position"
    t.integer  "idx"
    t.string   "sequence"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["lab_id"], name: "index_tracers_on_lab_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password"
    t.string   "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "auth_token"
    t.integer  "lab_id"
    t.index ["lab_id"], name: "index_users_on_lab_id", using: :btree
  end

  add_foreign_key "barcodes", "labs"
  add_foreign_key "medics", "labs"
  add_foreign_key "patients", "labs"
  add_foreign_key "patients", "medics"
  add_foreign_key "sequence_runs", "labs"
  add_foreign_key "sequencing_samples", "sequence_runs"
  add_foreign_key "tests", "labs"
  add_foreign_key "tests", "patients"
  add_foreign_key "tracers", "labs"
  add_foreign_key "users", "labs"
end
