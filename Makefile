VERSION=$(shell cat version)

all:  push-image

build-image:
	echo Version $(VERSION)
	docker build --build-arg VERSION=$(VERSION) \
	-t docker.appcenter.ro/hello-rails:$(VERSION) .

push-image: build-image
	docker push docker.appcenter.ro/hello-rails:$(VERSION)

deploy:	deploy-dev

deploy-dev:
	kubectl get -f ./k8s-deployment.yaml ; \
	if [ $$? -ne 0 ] ; \
	then \
		kubectl create -f ./k8s-deployment.yaml ; \
	fi ; \
	kubectl set image deployment/hello-rails rails=docker.appcenter.ro/hello-rails:`cat version`
