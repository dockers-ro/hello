FROM ruby:2.3

RUN apt-get update -qq && apt-get install -y nodejs

WORKDIR /tmp
ADD Gemfile Gemfile
ADD Gemfile.lock Gemfile.lock
RUN bundle install

ENV app /app
RUN mkdir $app
WORKDIR $app
COPY . $app

EXPOSE 3000

#RUN bundle install

#CMD ["/bin/sh", "-c", "cd /app ; rails s"]
CMD rails db:migrate && rails s -b 0.0.0.0
